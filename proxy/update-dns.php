<?php

echo "start updating dns in default.conf ... " . PHP_EOL;

/**
 * server {
listen 80;
server_name home.test;

location / {
proxy_pass http://app_nginx_8001;
}
}

 */

$dns = require_once "dns.php";

// open dns.php
$defaultConf = "";

echo "---------------------------------". PHP_EOL;

foreach ($dns as $subDomain => $container)
{
    $defaultConf .= "server {
    listen 80;
    server_name $subDomain;
    location / {
        proxy_pass http://$container;
    }
}
";

    echo $subDomain . " -> " . $container . PHP_EOL;
}

$defaultConf = "server {
    listen 80 default_server;

    server_name _;
    root /usr/share/nginx/html;

    charset UTF-8;

    error_page 404 /backend-not-found.html;
    location = /backend-not-found.html {
        allow all;
    }
    location / {
        return 404;
    }
}
" . $defaultConf;

echo "---------------------------------". PHP_EOL;

// update default.conf
try{
    file_put_contents(__DIR__ . "/default.conf", $defaultConf);
} catch (Exception $e)
{
    die("error update default.conf : " . $e->getMessage() );
}

echo "dns updated in default.conf" . PHP_EOL;
