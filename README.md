### Reverse-proxy docker nginx:

infos/tutorials: 
https://www.grottedubarbu.fr/docker-nginx-reverse-proxy/
https://phoenixnap.com/kb/docker-nginx-reverse-proxy

ci/cd gitlab:
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-continuous-deployment-pipeline-with-gitlab-ci-cd-on-ubuntu-18-04

**NB: improvement**
   - app_network can be created on the fly, on first container creation )
   - add https for container 
    
##### Steps:

1/ create network (improvement : see above ^):

```
docker network create app_network || true
```
This network will be used by reverse-proxy to retrieve all containers, themselves created in this network

2/ create an application container on this network **( /!\ it's a disctinct project )**:

   - use this docker-compose.yml template:
 
```
version: '3.3'
services:
  app:
    container_name: app_nginx_8001 # <- container name used by nginx proxy/default.conf to fetch it
    image: nginx:1.9
    volumes:
      - .:/usr/share/nginx/html/
    expose: # or ports:
      - "80" # or - "8000:80"
    networks:
      - app_network # <- network for all containers

networks:
  app_network: # <- network for all containers
    external: true
```

3/ start this application container by its own docker-compose.yml:

   - for example:
        
```
cd myapp/

docker-compose -f docker-compose.yml up -d
```

Now you have a container named **app_nginx_8001** available on **app_network** shared network
    

4/ connect this container to a sub-domain with the reverse-proxy nginx default.conf

NB: 1st server conf is the default app if reverse-proxy can't route to an app
```
server {
    listen 80 default_server;

    server_name _;
    root /usr/share/nginx/html;

    charset UTF-8;

    error_page 404 /backend-not-found.html;
    location = /backend-not-found.html {
        allow all;
    }
    location / {
        return 404;
    }
}

...

server {
    listen 80;
    server_name app1.domain.com;

    location / {
        proxy_pass http://app_nginx_8001;
    }
}

...

server {
    ...
}
```

5/ start reverse proxy:
```
docker-compose -f proxy/docker-compose.yml -p proxy up -d
```

Now, if you go to **http://app1.domain.com**, the reverse-proxy may route to your **app_nginx_8001** container


6/ add new container/application MANUALLY:
   - start/create a new container/application as described above
   
   - add new domain conf and container name on reverse-proxy nginx default.conf
```      
server {
    listen 80;
    server_name app2.domain.com;

    location / {
        proxy_pass http://app_nginx_8002;
    }
}
```       
   - reload conf :        
```
docker-compose -f proxy/docker-compose.yml -p proxy up -d --build
```
Now, if you go to **http://app2.domain.com**, the reverse-proxy may route to your **app_nginx_8002** container


7/ add new container/application WITH SCRIPT:
   - start/create a new container/application as described above

   - check running containers
    
        ```docker ps```
   - modify dns.php : list of sub-domain:container_name
   
        ```make conf```
   - reload new conf
   
        ```make reload```
