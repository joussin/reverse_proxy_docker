start: ##@Docker start proxy
	docker network create app_network || true
	docker-compose -f proxy/docker-compose.yml up -d --build

conf: ##@Docker update conf
	vim proxy/dns.php

reload: ##@Docker reload conf
	php proxy/update-dns.php
	docker-compose -f proxy/docker-compose.yml up -d --build

# kill: ##@Docker destroy all
# 	docker stop $(docker ps -a -q)
# 	docker rm $(docker ps -a -q)
# 	docker system prune -a
